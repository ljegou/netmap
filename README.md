# NetMap

**Application HTML combinant un graphe nœuds-liens et une carte de flux interactive.**

Demos :

- [Flux de ferraille, MuCEM](http://www.geotests.net/test/net_map/fer_mep.html)
- [Collaborations scientifiques, WoS, 2000, 2007 et 2013](http://www.geotests.net/test/net_map/science_mep.html)

Composants utilisés :

- Vis.JS : http://visjs.org/ (via le package R VisNetwork)
- D3JS.org : https://d3js.org/

**Auteurs :**

Marion Maisonobe, UMR Géographies-Cités, marion.maisonobe[@]cnrs.fr  
Laurent Jégou, LISST Université de Toulouse-Jean Jaurès, jegou[@]univ-tlse2.fr

**Licence :** [CeCILL-C](http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html)